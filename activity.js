db.fruits.aggregate([

	{$match: {supplier:"Red Farms Inc."}},
	{$count: "amountOfItemsOfRedFarms" }

])

db.fruits.aggregate([

	{$match: {supplier:"Green Farming and Canning"}},
	{$count: "amountOfItemsOfGreenFarms" }
	
])

db.fruits.aggregate([

	{$match: {onSale: true}},
	{$group: {_id: "$supplier", avgPrice: {$avg:"$price"}}}

])

db.fruits.aggregate([

	{$match: {onSale: true}},
	{$group: {_id: "$supplier", maxPrice: {$max:"$price"}}}

])

db.fruits.aggregate([

	{$match: {onSale: true}},
	{$group: {_id: "$supplier", minPrice: {$min:"$price"}}}

])